import java.util.Scanner;

public class CseLabFinder {
    public static void getCseLab() {
        int x, y, z, n;
        Scanner myObj = new Scanner(System.in);

        x = myObj.nextInt();
        y = myObj.nextInt();
        z = myObj.nextInt();
        n = myObj.nextInt();

        int xRem = x - n;
        int yRem = y - n;
        int zRem = z - n;

        if (xRem >= 0) {
            if (yRem >= 0) {
                if (zRem >= 0) {
                    if (xRem < yRem && xRem < zRem) {
                        System.out.println("L1");
                    } else if (yRem < xRem && yRem < zRem) {
                        System.out.println("L2");
                    } else {
                        System.out.println("L3");
                    }
                } else {
                    if (xRem < yRem) {
                        System.out.println("L1");
                    } else if (yRem < xRem) {
                        System.out.println("L2");
                    }
                }
            } else {
                if (zRem >= 0) {
                    if (xRem < zRem) {
                        System.out.println("L1");
                    } else {
                        System.out.println("L3");
                    }
                } else {
                    System.out.println("L1");
                }
            }
        } else {
            if (yRem >= 0) {
                if (zRem >= 0) {
                    if (yRem < zRem) {
                        System.out.println("L2");
                    } else {
                        System.out.println("L1");
                    }
                } else {
                    System.out.println("L2");
                }
            } else {
                System.out.println("L3");
            }
        }
    }
}
